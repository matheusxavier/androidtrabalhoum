package br.com.matheus.trabalhoum;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class BiggestNumberGame extends AppCompatActivity {

    Random rand = new Random();
    ArrayList<Integer> numbers = new ArrayList<Integer>();
    int count = 1, correct = 0;
    TextView numbersText;
    EditText responseText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_biggest_number_game);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        numbersText = (TextView) findViewById(R.id.numbersTextView);
        responseText = (EditText) findViewById(R.id.responseEditText);

        this.newGame();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    private void newGame() {

        numbers = new ArrayList<Integer>();
        for (int i = 0; i < 3; i++) {
            int n = rand.nextInt(10);
            numbers.add(n);
        }

        String text = numbers.get(0) + "\t" + numbers.get(1) + "\t" + numbers.get(2);
        numbersText.setText(text);
    }

    public void answer(View view) {

        String response = responseText.getText().toString();
        Collections.sort(numbers, Collections.<Integer>reverseOrder());
        String correctResponse = "" + numbers.get(0) + numbers.get(1) + numbers.get(2);

        if (response.equals(correctResponse)) {
            if (count <= 4) this.presentDialog("Acertou!", "Parabéns, você acertou!");
            correct++;
        } else {
            if (count <= 4) this.presentDialog("Errou!", "A resposta certa era " + correctResponse);
        }

        count++;
        responseText.setText("");

        if (count <= 5) {
            this.newGame();
        } else {
            this.presentFinalDialog();
        }
    }

    private void presentDialog(String title, String message) {
        AlertDialog alert;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        alert = builder.create();
        alert.show();
    }

    private void presentFinalDialog() {
        AlertDialog alert;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Fim do Jogo!");
        builder.setMessage("Sua nota final: " + (correct / 5.0) * 100)
                .setPositiveButton("Jogar Novamente", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        count = 1;
                        correct = 0;
                        newGame();
                    }
                })
                .setNegativeButton("Voltar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        onBackPressed();
                    }
                });
        alert = builder.create();
        alert.show();
    }
}
