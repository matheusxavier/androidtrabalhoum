package br.com.matheus.trabalhoum;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Random;

public class ArithmeticGame extends AppCompatActivity {

    Random rand = new Random();
    int n1, n2, op, count = 1, correct = 0;
    TextView questionText;
    EditText responseText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arithmetic_game);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        responseText = (EditText) findViewById(R.id.responseEditText);
        questionText = (TextView) findViewById(R.id.questionTextView);

        this.newGame();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    private void newGame() {

        op = rand.nextInt(2);
        op += 1;

        n1 = rand.nextInt(10);
        n2 = rand.nextInt(10);

        String operator = op == 1 ? " - " : " + ";
        questionText.setText(n1 + operator + n2);
    }

    public void answer(View view) {

        Integer response = Integer.parseInt(responseText.getText().toString());
        int correctResponse;

        switch(op) {
            case 1:
                correctResponse = n1 - n2;
                break;
            default:
                correctResponse = n1 + n2;
                break;
        }

        if (response == correctResponse) {
            if (count <= 4) this.presentDialog("Acertou!", "Parabéns, você acertou!");
            correct++;
        } else {
            if (count <= 4) this.presentDialog("Errou!", "A resposta certa era " + correctResponse);
        }

        count++;
        responseText.setText("");

        if (count <= 5) {
            this.newGame();
        } else {
            this.presentFinalDialog();
        }
    }

    private void presentDialog(String title, String message) {
        AlertDialog alert;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        alert = builder.create();
        alert.show();
    }

    private void presentFinalDialog() {
        AlertDialog alert;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Fim do Jogo!");
        builder.setMessage("Sua nota final: " + (correct / 5.0) * 100)
                .setPositiveButton("Jogar Novamente", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        count = 1;
                        correct = 0;
                        newGame();
                    }
                })
                .setNegativeButton("Voltar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        onBackPressed();
                    }
                });
        alert = builder.create();
        alert.show();
    }
}
