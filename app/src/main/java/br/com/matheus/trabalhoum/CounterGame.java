package br.com.matheus.trabalhoum;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Random;

public class CounterGame extends AppCompatActivity {

    Button opt1, opt2, opt3;
    Random rand = new Random();
    int currentNumber, count = 1, correct = 0;
    ArrayList<Integer> alreadyUsed = new ArrayList<Integer>();

    View.OnClickListener onClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            Button button = (Button) v;
            Integer value = Integer.parseInt(button.getText().toString());

            if (value == currentNumber) {
                correct++;
                if (count <= 4) CounterGame.this.presentDialog("Acertou", "Parabéns, você acertou!");
            } else  {
                if (count <= 4) CounterGame.this.presentDialog("Errou :(", "O número correto era " + currentNumber + "!");
            }

            if (count < 5) {
                count++;
                CounterGame.this.newGame();
            } else {
                CounterGame.this.presentFinalDialog();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_counter_game);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        opt1 = (Button) findViewById(R.id.option1);
        opt2 = (Button) findViewById(R.id.option2);
        opt3 = (Button) findViewById(R.id.option3);

        this.newGame();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    private void newGame() {

        ImageView showNumber = (ImageView) findViewById(R.id.imageView);

        ArrayList<Integer> generated = this.generateNumbers();
        this.currentNumber = generated.get(0);
        this.alreadyUsed.add(this.currentNumber);

        String resourceName = "item_" + this.currentNumber;
        int resourceId = this.getResources().getIdentifier(resourceName, "drawable", getApplicationInfo().packageName);

        showNumber.setImageResource(resourceId);

        this.assignNumbers(generated);
    }

    private ArrayList<Integer> generateNumbers() {
        ArrayList<Integer> generated = new ArrayList<Integer>();
        for (int i = 0; i < 3; i++) {
            int n = rand.nextInt(10);
            n += 1;

            while (generated.indexOf(n) != -1) {
                n = rand.nextInt(10);
                n += 1;
            }

            if (i == 0 && alreadyUsed.indexOf(n) != -1) {
                while (alreadyUsed.indexOf(n) != -1) {
                    n = rand.nextInt(10);
                    n += 1;
                }
            }

            generated.add(n);
        }

        return generated;
    }

    private void assignNumbers(ArrayList<Integer> arr) {

        ArrayList<Integer> assigned = new ArrayList<Integer>();

        int n = rand.nextInt(arr.size());

        assigned.add(n);
        opt1.setText(arr.get(n).toString());
        opt1.setOnClickListener(onClick);

        while (assigned.indexOf(n) != -1) {
            n = rand.nextInt(arr.size());
        }

        assigned.add(n);
        opt2.setText(arr.get(n).toString());
        opt2.setOnClickListener(onClick);

        while (assigned.indexOf(n) != -1) {
            n = rand.nextInt(arr.size());
        }

        assigned.add(n);
        opt3.setText(arr.get(n).toString());
        opt3.setOnClickListener(onClick);
    }

    private void presentDialog(String title, String message) {
        AlertDialog alert;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        alert = builder.create();
        alert.show();
    }

    private void presentFinalDialog() {
        AlertDialog alert;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Fim do Jogo!");
        builder.setMessage("Sua nota final: " + (correct / 5.0) * 100)
                .setPositiveButton("Jogar Novamente", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        count = 1;
                        correct = 0;
                        alreadyUsed = new ArrayList<Integer>();
                        newGame();
                    }
                })
                .setNegativeButton("Voltar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        onBackPressed();
                    }
                });
        alert = builder.create();
        alert.show();
    }

}