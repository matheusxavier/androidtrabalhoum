package br.com.matheus.trabalhoum;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onCounterClick(View view) {
        Intent it = new Intent(this, CounterGame.class);
        startActivity(it);
    }

    public void onArithmeticClick(View view) {
        Intent it = new Intent(this, ArithmeticGame.class);
        startActivity(it);
    }

    public void onBiggestNumberClick(View view) {
        Intent it = new Intent(this, BiggestNumberGame.class);
        startActivity(it);
    }

}
